require('shelljs/global');
var http = require('http');
var fs = require('fs');

var execCmd = function (cmd) {
    if (exec(cmd).code !== 0) {
        echo("Command `" + cmd + "` failed. Please check the logs");
        exit(1);
    }
}

var sedFile = function (regex, value, fileName) {
    if (fs.existsSync(fileName)) {
        sed('-i', regex, value, fileName);
    }
}

var download = function (url, destDir, destsFile, cb) {
  var filePath = destDir + '/' + destFile;
  mkdir('-p', destDir);
  rm('-f', filePath);

  var file = fs.createWriteStream(filePath);

  var options = {
     hostname: repoHost(),
     port: 80,
     path: url,
     method: 'GET',
     headers: {
         'Content-Type': 'application/json',
     }
   }

  console.log('Downloading ' + url + '...');

  var request = http.get(options, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close(cb);
    });
  });
}

var repoHost = function () {
    //host name
    return 'bitbucket.org';
}

var repoBaseUrl = function () {
    var host = repoHost();
    return 'https://' + host + '/aliftechnologies/wb-tooling/browse';
}

var getConfig = function () {
  return require('../tmp/' + getCurrentEnv() + '.json');
}

var getCurrentEnv = function () {
  //Environment related codes.. environment may vary depending upon requirement
  //so addons will be loaded depending on environment
    return 'alif_dev';
}

module.exports = repoBaseUrl;
global['repoBaseUrl'] = repoBaseUrl;

module.exports = getConfig;
global['getConfig'] = getConfig;

module.exports = execCmd;
global['execCmd'] = execCmd;

module.exports = download;
global['download'] = download;

module.exports = sedFile;
global['sedFile'] = sedFile;

module.exports = getCurrentEnv;
global['getCurrentEnv'] = getCurrentEnv;